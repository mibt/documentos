# MIBT - Documentação

Neste repositório estão concentrados os documentos gerados para o projeto. No intuito de manter uma separação entre eles, cada documento esta localizado em uma *branch* independente com o seu nome correspondente.

### Documentos armazenados:

- Relatório IOTA
- Estudo de Sistemas Gerenciadores de Indicadores

### Criação de novos documentos

Para simplificar e possibilitar uma padronização, a *branch template* oferece um modelo básico para criação de documentos.